# Cloak Datastructures Library

The Cloak library aims to provide some extra datastructures to Python, to complement the built-in data structures.
Currently the Cloak library provides usable Option and Try types for Python developers.

## Examples

#### Using Option types

```

    from __future__ import print_function
    from cloak.option import lift_exception_to_option_functor
    try:
        from queue import Queue, Empty
    except ImportError:
        from Queue import Queue, Empty

    # Print anything in our queue, if there's something to receive.
    data_queue = Queue()
    opt_queue_get = lift_exception_to_option_functor(data_queue.get, (Empty))
    data_queue.put("hello!")
    opt_message = opt_queue_get(blocking=True, timeout=1)
    # There was a message in the queue, so it will be printed.
    opt_message.map(print).or_else(print, "No message received!")
    
    # Similar, run a length calculation or return None if no message
    data_queue.put("test message")
    msg_len = opt_queue_get(blocking=True, timeout=1).map(len).get_or_else(None)
    if msg_len is None:
        print("No message received!")
    else:
        print("Message of length {0} received.".format(msg_len))
    
    opt_message = opt_queue_get(blocking=True, timeout=1)
    # No message this time, so "No message received!" will be printed.
    opt_message.map(print).or_else(print, "No message received!")
    

```

#### Using Try types

```

    from __future__ import print_function
    from cloak.try_type import Try

    class OddIntException(Exception): pass

    def may_throw(an_int):
        an_int = int(an_int)
        if an_int % 2:
            raise OddIntException("{0} is odd!".format(an_int))
        else:
            return an_int * 2
            
    def print_number(num):
        print("Seen: {0}".format(num))
        return num
    
    default_number = 0
    non_odd_number = 2
    for num in [None, 1, 2, 3, 4]:
        try_may_throw = Try.apply(may_throw, num).recover(
            (ValueError, lambda e: default_number), # If a non-number was given, just substitute the default number.
            (OddIntException, lambda e: non_odd_number) # If an odd number was given, just substitute an even number
        ).map(print_number).or_else(print, "Should never be here!")

```

## API Docs

Read the docs at: http://cloak.readthedocs.org