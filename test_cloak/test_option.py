from unittest import TestCase
from assertpy import assert_that
from multiprocessing import Queue, Pipe
try:
    from queue import Empty
except ImportError:
    from Queue import Empty

from cloak.option import Some, Nothing, nil, Option, lift_exception_to_option_functor, lift_option_functor
from cloak.monadic import InnerValueNotContainerTypeException, NoSuchElementException


def compose(*funcs):
    def composed(arg):
        result = arg
        for func in funcs:
            result = func(result)
        return result
    return composed


class OptionUnitTests(TestCase):
    def test_monad_laws(self):
        """
        Test to verify that the Option data structures reasonably obey the monad laws.
        """
        def plus_two(value):
            return value + 2

        assert_that(Some(1).bind(Some)).is_equal_to(Some(1))
        assert_that(nil.bind(Some)).is_equal_to(nil)
        assert_that(Some(1).bind(plus_two)).is_equal_to(plus_two(1))
        assert_that(Some("test").bind(Some)).is_equal_to(Some("test"))
        assert_that(nil.bind(Some)).is_equal_to(nil)
        assert_that(str(Some(5).bind(plus_two))).is_equal_to(compose(plus_two, str)(5))

    def test_immutability(self):
        """
        Test to verify that the Some data structure is "reasonably" immutable, and can't be simply mutated.
        """
        def mutate_value(opt):
            opt._value = None

        a = Some(1)
        self.assertRaises(TypeError, mutate_value, a)

    def test_some_methods(self):
        """
        Test to verify that the methods for the Some[T] data type behave as expected.
        """
        def trigger_assert():
            raise AssertionError("Should not ever be called!")

        def raise_standard_error(_):
            raise ValueError

        test_value = 123
        opt = Some(test_value)
        nested_opt = Some(opt)
        assert_that(opt.is_empty).is_false()
        assert_that(opt.is_defined).is_true()
        assert_that(opt.map(lambda val: val + 5)).is_equal_to(Some(test_value + 5))
        assert_that(nested_opt.flatten()).is_equal_to(opt)
        assert_that(opt.get()).is_equal_to(test_value)
        assert_that(opt.get_or_else(not test_value)).is_equal_to(test_value)
        assert_that(opt.join()).is_equal_to(test_value)
        assert_that(opt.or_else(trigger_assert)).is_equal_to(opt)
        assert_that(opt.exists(lambda val: val == test_value)).is_true()
        assert_that(opt.exists(lambda val: val != test_value)).is_false()
        assert_that(opt.filter(lambda val: val == test_value)).is_equal_to(opt)
        assert_that(opt.filter(lambda val: val != test_value)).is_equal_to(nil)
        assert_that(opt.bind(lambda val: val + 20)).is_equal_to(test_value + 20)
        assert_that(Some([1]).bind(lambda val: val)).is_equal_to([1])
        self.assertRaises(ValueError, opt.for_each, raise_standard_error)
        self.assertRaises(InnerValueNotContainerTypeException, opt.flatten)

    def test_nil_methods(self):
        """
        Test to verify that the methods for the nil/Nothing object behave as expected.
        """
        def trigger_assert():
            raise AssertionError("Should not ever be called!")

        def raise_standard_error(_):
            raise ValueError

        assert_that(nil.is_empty).is_true()
        assert_that(nil.is_defined).is_false()
        assert_that(nil.map(lambda val: val + 5)).is_equal_to(nil)
        assert_that(nil.flatten()).is_equal_to(nil)
        assert_that(nil.get_or_else(25)).is_equal_to(25)
        assert_that(nil.join()).is_equal_to(nil)

        assert_that(nil.exists(lambda val: val == nil)).is_false()
        assert_that(nil.exists(lambda val: val != nil)).is_false()
        assert_that(nil.filter(lambda val: val == nil)).is_equal_to(nil)
        assert_that(nil.filter(lambda val: val != nil)).is_equal_to(nil)
        assert_that(nil.bind(lambda val: val + 20)).is_equal_to(nil)

        nil.for_each(raise_standard_error)
        self.assertRaises(AssertionError, nil.or_else, trigger_assert)
        self.assertRaises(NoSuchElementException, nil.get)

    def test_option_constructors(self):
        """
        Test to make sure the Option constructors return the right types.
        """
        assert_that(Option.unit(1)).is_equal_to(Some(1))
        assert_that(Option.zero()).is_equal_to(nil)
        assert isinstance(nil, Nothing)
        assert isinstance(nil, Option)
        assert isinstance(Some(1), Option)

    def test_lift_functors(self):
        """
        Test to make sure the lift functors modify the decorated functions correctly.
        """
        def may_raise_something(a_value):
            if a_value == 0:
                return True
            elif a_value == 1:
                raise ValueError
            elif a_value == 2:
                raise IOError
            else:
                raise KeyError

        lifted = lift_exception_to_option_functor(may_raise_something, [ValueError])
        assert_that(lifted(0)).is_equal_to(Some(True))
        assert_that(lifted(1)).is_equal_to(nil)
        self.assertRaises(IOError, lifted, 2)
        self.assertRaises(KeyError, lifted, 3)

        def add(a, b):
            return a + b

        lifted_add = lift_option_functor(add)
        assert_that(lifted_add(1, nil)).is_equal_to(nil)
        assert_that(lifted_add(nil, 5)).is_equal_to(nil)
        assert_that(lifted_add(nil, Some(2))).is_equal_to(nil)
        assert_that(lifted_add(1, 2)).is_equal_to(Some(3))
        assert_that(lifted_add(Some(1), 5)).is_equal_to(Some(6))
        assert_that(lifted_add(10, Some(2))).is_equal_to(Some(12))
        assert_that(lifted_add(Some(-1), Some(1))).is_equal_to(Some(0))

    def test_lift_on_real_functions(self):
        """
        Test to make sure the lifter functions behave appropriately in a real-ish working scenario.
        """
        parent_conn, child_conn = Pipe()
        parent_conn_opt_recv = lift_exception_to_option_functor(parent_conn.recv, (EOFError,))
        parent_conn_opt_send = lift_option_functor(parent_conn.send)

        child_conn.send("hello")
        assert_that(parent_conn_opt_recv().get()).is_equal_to("hello")
        parent_conn_opt_send("test1")
        assert_that(child_conn.poll(1)).is_true()
        assert_that(child_conn.recv()).is_equal_to("test1")
        parent_conn_opt_send(Some("test2"))
        assert_that(child_conn.poll(1)).is_true()
        assert_that(child_conn.recv()).is_equal_to("test2")

        parent_conn_opt_send(nil)
        assert_that(child_conn.poll(1)).is_false()

        child_conn.close()
        assert_that(parent_conn_opt_recv()).is_equal_to(nil)
        parent_conn.close()

        queue = Queue()
        opt_get_queue = lift_exception_to_option_functor(queue.get, (Empty,), apply_wraps=True)
        queue.put("world")
        assert_that(opt_get_queue(block=True, timeout=0.5).get()).is_equal_to("world")
        assert_that(opt_get_queue(block=True, timeout=0.5)).is_equal_to(nil)
        assert_that(opt_get_queue(block=False)).is_equal_to(nil)
