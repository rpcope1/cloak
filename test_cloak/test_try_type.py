from unittest import TestCase
from assertpy import assert_that
from multiprocessing import Pipe

from cloak.option import Some, nil
from cloak.try_type import Try, Success, Failure, lift_try_functor
from cloak.monadic import NoSuchElementException


def build_test_trap():
    shared_bool = [False]

    def trigger_trap(*args):
        shared_bool[0] = True
        return args[0] if args else None

    def read_trap():
        return shared_bool[0]

    return trigger_trap, read_trap


class TryUnitTests(TestCase):
    def test_basic_try_behavior(self):
        """
        Test to verify basic behavior of Try's apply function behaves in an expected manner.
        """
        def raise_an_exception():
            raise ValueError

        trigger_trap, read_trap = build_test_trap()

        try_result = Try.apply(lambda: True)
        assert_that(isinstance(try_result, Success)).is_true()
        assert_that(try_result.get()).is_equal_to(True)
        assert_that(try_result).is_equal_to(Try.unit(True))
        assert_that(read_trap()).is_false()
        try_result.map(trigger_trap)
        assert_that(read_trap()).is_true()

        trigger_trap, read_trap = build_test_trap()

        try_result = Try.apply(raise_an_exception)
        assert_that(isinstance(try_result, Failure)).is_true()
        assert_that(try_result.exception).is_instance_of(ValueError)
        assert_that(try_result).is_equal_to(Try.zero(try_result.exception))
        assert_that(read_trap()).is_false()
        try_result.map(trigger_trap)
        assert_that(read_trap()).is_false()

    def test_immutability(self):
        """
        Test to verify that the attributes on both Success and Failure are immutable.
        """
        def alter_inner_value(success, new_value):
            success._result = new_value

        def alter_exception(failure, new_exception):
            failure.exception = new_exception

        success = Success(True)
        assert_that(success.get()).is_true()
        self.assertRaises(TypeError, alter_inner_value, success, "wat")
        assert_that(success.get()).is_true()

        failure = Failure(ValueError())
        assert_that(failure.exception).is_instance_of(ValueError)
        self.assertRaises(TypeError, alter_exception, failure, IOError())
        assert_that(failure.exception).is_instance_of(ValueError)

    def test_subtype_constructor_methods(self):
        """
        Test to verify that the subtype constructor methods (unit/zero) actually map to the correct classes.
        """
        assert_that(Success(True)).is_equal_to(Try.unit(True))
        exception = ValueError()
        assert_that(Failure(exception)).is_equal_to(Try.zero(exception))

    def test_success_methods(self):
        """
        Test to verify that all of the Success type methods work as expected.
        """

        test_success_one = Success(1)
        test_success_two = Success(["foo"])

        assert_that(test_success_one.is_success).is_true()
        assert_that(test_success_two.is_success).is_true()
        assert_that(test_success_one.is_failure).is_false()
        assert_that(test_success_two.is_failure).is_false()

        trigger_trap, read_trap = build_test_trap()
        assert_that(read_trap()).is_false()
        assert_that(test_success_one.or_else(trigger_trap, "test")).is_equal_to(test_success_one)
        assert_that(read_trap()).is_false()

        trigger_trap, read_trap = build_test_trap()
        assert_that(read_trap()).is_false()
        assert_that(test_success_one.or_else_with(trigger_trap, "test")).is_equal_to(test_success_one)
        assert_that(read_trap()).is_false()

        trigger_trap, read_trap = build_test_trap()
        assert_that(read_trap()).is_false()
        assert_that(test_success_two.or_else(trigger_trap, "test")).is_equal_to(test_success_two)
        assert_that(read_trap()).is_false()

        trigger_trap, read_trap = build_test_trap()
        assert_that(read_trap()).is_false()
        assert_that(test_success_two.or_else_with(trigger_trap, "test")).is_equal_to(test_success_two)
        assert_that(read_trap()).is_false()

        test_opt_one = test_success_one.to_option()
        assert_that(isinstance(test_opt_one, Some)).is_true()
        assert_that(test_success_one.get()).is_equal_to(test_opt_one.get())

        trigger_trap, read_trap = build_test_trap()
        assert_that(read_trap()).is_false()
        assert_that(test_success_one.map(trigger_trap)).is_equal_to(test_success_one)
        assert_that(read_trap()).is_true()

        trigger_trap, read_trap = build_test_trap()
        assert_that(read_trap()).is_false()
        assert_that(test_success_one.filter(trigger_trap)).is_equal_to(test_success_one)
        assert_that(read_trap()).is_true()

        assert_that(test_success_one.filter(lambda _: False).is_failure).is_true()
        assert_that(test_success_one.filter(lambda _: True).is_success).is_true()

        assert_that(test_success_one.join()).is_equal_to(test_success_one.get())
        assert_that(test_success_one.get_or_else(None)).is_equal_to(test_success_one.get())

        on_success_trigger, read_on_success_trap = build_test_trap()
        on_failure_trigger, read_on_failure_trap = build_test_trap()
        assert_that(read_on_failure_trap()).is_false()
        assert_that(read_on_success_trap()).is_false()
        assert_that(test_success_one.transform(on_success_trigger, on_failure_trigger)).\
            is_equal_to(test_success_one)
        assert_that(read_on_failure_trap()).is_false()
        assert_that(read_on_success_trap()).is_true()

        recover_trigger, read_recover_trap = build_test_trap()
        assert_that(test_success_one.recover(
            (BaseException, recover_trigger)
        )).is_equal_to(test_success_one)
        assert_that(read_recover_trap()).is_false()

        recover_trigger, read_recover_trap = build_test_trap()
        assert_that(test_success_one.recover_if(
            (lambda e: True, recover_trigger)
        )).is_equal_to(test_success_one)
        assert_that(read_recover_trap()).is_false()

    def test_failure_methods(self):
        """
        Test to verify that Failure methods behave as expected.
        """
        test_failure_one = Failure(ValueError("A failure"))
        test_failure_two = Failure(TypeError("Another failure"))

        assert_that(test_failure_one.is_failure).is_true()
        assert_that(test_failure_two.is_success).is_false()

        trigger, read_trap = build_test_trap()
        test_failure_one.or_else(trigger)
        assert_that(read_trap()).is_true()

        assert_that(test_failure_two.to_option()).is_equal_to(nil)

        trigger, read_trap = build_test_trap()
        assert_that(test_failure_one.map(trigger)).is_equal_to(test_failure_one)
        assert_that(read_trap()).is_false()

        trigger, read_trap = build_test_trap()
        assert_that(test_failure_one.filter(trigger)).is_equal_to(test_failure_one)
        assert_that(read_trap()).is_false()

        assert_that(test_failure_two.join()).is_equal_to(test_failure_two)

        self.assertRaises(NoSuchElementException, test_failure_two.get)

        assert_that(test_failure_one.get_or_else(None)).is_none()

        assert_that(test_failure_one.or_else(lambda: True)).is_true()
        assert_that(test_failure_one.or_else_with(lambda: True)).is_equal_to(Success(True))

        on_success_trigger, read_on_success_trap = build_test_trap()
        on_failure_trigger, read_on_failure_trap = build_test_trap()
        test_failure_one.transform(on_success_trigger, on_failure_trigger)
        assert_that(read_on_success_trap()).is_false()
        assert_that(read_on_failure_trap()).is_true()

        trigger_one, read_trigger_one = build_test_trap()
        trigger_two, read_trigger_two = build_test_trap()

        test_failure_one.recover(
            (IOError, trigger_one),
            (ValueError, trigger_two)
        )
        assert_that(read_trigger_one()).is_false()
        assert_that(read_trigger_two()).is_true()

        trigger_one, read_trigger_one = build_test_trap()
        trigger_two, read_trigger_two = build_test_trap()

        test_failure_two.recover_if(
            (lambda e: isinstance(e, ValueError), trigger_one),
            (lambda e: isinstance(e, TypeError), trigger_two)
        )
        assert_that(read_trigger_one()).is_false()
        assert_that(read_trigger_two()).is_true()

    def test_lift_try_functor(self):
        """
        Test to ensure the lift try functor seamlessly functions when
        Try/Success/Failure are used.
        """

        success_one = Success(True)
        failure_one = Failure(ValueError())
        other_arg = 1

        trigger, read_trap = build_test_trap()
        lifted_trigger = lift_try_functor(trigger)
        result = lifted_trigger(success_one, other_arg)
        assert_that(isinstance(result, Success)).is_true()
        assert_that(result).is_equal_to(success_one)
        assert_that(read_trap()).is_true()

        trigger, read_trap = build_test_trap()
        lifted_trigger = lift_try_functor(trigger)
        result = lifted_trigger(success_one, failure_one)
        assert_that(isinstance(result, Failure)).is_true()
        assert_that(result).is_equal_to(failure_one)
        assert_that(read_trap()).is_false()

    def test_lift_on_real_functions(self):
        """
        Test to make sure the lifter functions behave appropriately in a real-ish working scenario.
        """
        parent_conn, child_conn = Pipe()
        try_parent_conn_recv = lift_try_functor(parent_conn.recv)
        child_conn.send("test")
        assert_that(try_parent_conn_recv().get()).is_equal_to("test")
        child_conn.close()
        failed_recv = try_parent_conn_recv()
        assert_that(isinstance(failed_recv, Failure)).is_true()
        assert_that(isinstance(failed_recv, Success)).is_false()
        assert_that(failed_recv.exception).is_type_of(EOFError)
        parent_conn.close()
