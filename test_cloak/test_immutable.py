from unittest import TestCase
from assertpy import assert_that

from cloak.immutable import ImmutableSlotted, Immutable


class OptionUnitTests(TestCase):
    def test_immutable_slotted(self):
        """
        Test that the immutable slotted class has the actual correct shallow immutable behavior.
        """

        class MyStruct(ImmutableSlotted):
            __slots__ = ['foo', 'bar']

            def __init__(self, foo, bar):
                self.foo = foo
                self.bar = bar

        class MyStructBad(ImmutableSlotted):
            def __init__(self, foo, bar):
                self.foo = foo
                self.bar = bar

        s1 = MyStruct("a", "b")
        assert_that(s1.foo).is_equal_to("a")
        assert_that(s1.bar).is_equal_to("b")
        self.assertRaises(TypeError, setattr, s1, 'foo', 'c')
        self.assertRaises(TypeError, setattr, s1, 'bar', 'd')
        self.assertRaises(AttributeError, setattr, s1, 'baz', 'e')

        s2 = MyStructBad("a", "b")
        assert_that(s2.foo).is_equal_to("a")
        assert_that(s2.bar).is_equal_to("b")
        self.assertRaises(TypeError, setattr, s2, 'foo', 'c')
        self.assertRaises(TypeError, setattr, s2, 'bar', 'd')

        l1 = ["a", "b", "c"]
        s3 = MyStruct("a", l1)
        assert_that(s3.foo).is_equal_to("a")
        assert_that(s3.bar).is_equal_to(l1)
        s3.bar.append("d")
        assert_that(s3.bar).is_equal_to(l1).is_equal_to(["a", "b", "c", "d"])
        self.assertRaises(TypeError, setattr, s3, 'foo', 'c')
        self.assertRaises(TypeError, setattr, s3, 'bar', 'd')
        self.assertRaises(AttributeError, setattr, s3, 'baz', 'e')

    def test_immutable(self):
        """
        Test that the immutable slotted class has the actual correct immutable behavior.
        """

        class MyStruct(Immutable):
            def __init__(self, foo, bar):
                self.foo = foo
                self.bar = bar

        s1 = MyStruct("a", "b")
        assert_that(s1.foo).is_equal_to("a")
        assert_that(s1.bar).is_equal_to("b")
        self.assertRaises(TypeError, setattr, s1, 'foo', 'c')
        self.assertRaises(TypeError, setattr, s1, 'bar', 'd')

        l1 = ["a", "b", "c"]
        s3 = MyStruct("a", l1)
        assert_that(s3.foo).is_equal_to("a")
        assert_that(s3.bar).is_equal_to(l1)
        s3.bar.append("d")
        assert_that(s3.bar).is_equal_to(l1).is_equal_to(["a", "b", "c", "d"])
        self.assertRaises(TypeError, setattr, s3, 'foo', 'c')
        self.assertRaises(TypeError, setattr, s3, 'bar', 'd')