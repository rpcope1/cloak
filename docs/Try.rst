Cloak Try Type
=================

Class implementations
---------------------

.. autoclass:: cloak.try_type.Try
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: cloak.try_type.Success
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: cloak.try_type.Failure
    :members:
    :undoc-members:
    :show-inheritance:


Lifting functors
----------------

.. autofunction:: cloak.try_type.lift_try_functor
