.. Cloak documentation master file, created by
   sphinx-quickstart on Sat Jun  4 15:13:21 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cloak's documentation!
=================================

Cloak is an experimental set of advanced container datatypes, similar to those
found in languages such as Scala. These container datatypes promote use of 
functional paradigms such as composition, and allow the user to avoid behavior
like using exceptions for flow control in Python, if desired. This library
current provides an implementation of both an Option conatiner type, as well as
a Try container type. Additional, Immutable parent classes are provided to allow
users to create immutable objects without directly inheriting from a base datatype
like tuple.


Contents:

.. toctree::
   :maxdepth: 2

   Option
   Try
   Immutable

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

