Cloak Immutable Modifier
========================

Class implementations
---------------------

.. autoclass:: cloak.immutable.Immutable
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: cloak.immutable.ImmutableSlotted
    :members:
    :undoc-members:
    :show-inheritance:


