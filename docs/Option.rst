Cloak Option Type
=================

Class implementations
---------------------

.. autoclass:: cloak.option.Option
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: cloak.option.Some
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: cloak.option.Nothing
    :members:
    :undoc-members:
    :show-inheritance:

.. autodata:: cloak.option.nil

Lifting functors
----------------

.. autofunction:: cloak.option.lift_option_functor

.. autofunction:: cloak.option.lift_exception_to_option_functor
